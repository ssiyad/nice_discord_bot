import random
import re

from discord.ext import commands
from nice_discord_bot import bot


def probabilty(chance):
    '''Run function based on probabilty'''
    def wrapper(func):
        async def inner(*args, **kwargs):
            if chance <= random.randint(0, 100):
                return
            return await func(*args, **kwargs)
        return inner
    return wrapper


def is_text(func):
    '''Check if message contains any text'''
    async def inner(message):
        message.content = re.sub(r'\<\S+\>', '', message.content)
        message.content = re.sub(r'>.+\n', '', message.content)
        if not re.search(r'\S', message.content):
            return
        return await func(message)
    return inner


def not_command(func):
    '''Run the function only if not invoked by a command'''
    async def inner(message):
        ctx = await bot.get_context(message)
        if ctx.valid:
            return
        return await func(message)
    return inner
