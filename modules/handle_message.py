import re
import asyncio

from nice_discord_bot import bot, NICE_INVITE_LINK
from discord import Embed, Color
from discord.ext import commands
from database import inc_or_make_user
from checks import not_command


number_emojis = {}
for _number in range(0, 10):
    number_emojis[_number] = (
        str(_number) + "\N{variation selector-16}\N{combining enclosing keycap}"
    )


@not_command
async def message_handler(message):
    if message.mentions:
        if message.mentions[0].id == bot.user.id:
            await message.channel.send(
                embed=Embed(
                    title="Add me to yo server",
                    description=NICE_INVITE_LINK,
                    color=Color.dark_red(),
                )
            )

    # if not re.search('nice', message.content, re.IGNORECASE):
    #     return
    # await message.add_reaction('#️⃣')
    # for i in str(inc_or_make_user(message.guild.id, message.author.id)):
    #     await message.add_reaction(number_emojis[int(i)])
    #     await asyncio.sleep(0.4)
    # await message.add_reaction('⤴️')


@bot.listen()
async def on_message(message):
    await message_handler(message)
