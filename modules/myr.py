import re
import asyncio
import random

from chatterbot import ChatBot, response_selection, comparisons, filters
from chatterbot.trainers import ChatterBotCorpusTrainer
from chatterbot.trainers import ListTrainer
from nice_discord_bot import bot, unicode_chars, ottipp_list
from checks import is_text, not_command


_chatter_list = {}


class Chatter:
    def __init__(self):
        self.chatbot = ChatBot(
            "Nice",
            logic_adapters=[
                {
                    "import_path": "chatterbot.logic.BestMatch",
                    "statement_comparison_function": comparisons.SynsetDistance,
                    "response_selection_method": response_selection.get_first_response,
                },
                # "chatterbot.logic.MathematicalEvaluation",
            ],
            database_uri="sqlite:///corpus.db",
            preprocessors=[
                "chatterbot.preprocessors.clean_whitespace",
                "chatterbot.preprocessors.convert_to_ascii",
            ],
            filters=[filters.get_recent_repeated_responses],
        )

        self.trainer = ChatterBotCorpusTrainer(self.chatbot)
        # self.trainer.train('chatterbot.corpus.english')


async def sticker_handler(ctx):
    _message = await ctx.channel.history(limit=1, before=ctx.message).flatten()
    await ctx.message.delete()
    for i in ctx.invoked_with:
        await _message[0].add_reaction(unicode_chars[i])
        await asyncio.sleep(0.1)


# for _name in ottipp_list:
#     @bot.command(name=_name)
#     async def func(ctx):
#         await sticker_handler(ctx)


@is_text
@not_command
async def message_handler(message):
    if message.channel.id in _chatter_list:
        _chatter = _chatter_list[message.channel.id].chatbot
    else:
        _chatter = Chatter()
        _chatter_list[message.channel.id] = _chatter
        _chatter = _chatter.chatbot

    res = _chatter.get_response(message.content)

    if res.confidence < 0.85:
        return

    if 25 >= random.randint(0, 100):
        await message.channel.send(res.text)


@bot.listen()
async def on_message(message):
    await message_handler(message)
