from nice_discord_bot import bot


@bot.check
async def globally_block_dms(ctx):
    return ctx.guild is not None


@bot.check
async def block_own_msg(ctx):
    return ctx.message.author != bot.user or \
        ctx.author != bot.user
