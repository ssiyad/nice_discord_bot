import logging
import os
import datetime
import json
import unicode_emo

from discord.ext import commands


logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO)


LOGGER = logging.getLogger(__name__)


NICE_DISCORD_TOKEN = os.environ.get('NICE_DISCORD_TOKEN', None)
NICE_INVITE_LINK = os.environ.get('NICE_INVITE_LINK', None)


unicode_chars = unicode_emo.letters


with open('ottipp_list.json', 'r') as f:
    ottipp_list = json.load(f)


bot = commands.Bot(command_prefix='^ ', help_command=None)
