import os
from sqlalchemy import (
        create_engine,
        Column,
        BigInteger,
        String,
        Integer)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.orm.exc import NoResultFound


DB_URI = os.environ.get('DB_URI', 'sqlite:///db.db')


def start() -> scoped_session:
    engine = create_engine(DB_URI)
    BASE.metadata.bind = engine
    BASE.metadata.create_all(engine)
    return scoped_session(sessionmaker(bind=engine, autoflush=False))


try:
    BASE = declarative_base()
    SESSION = start()

except AttributeError as e:
    raise e


class NiceData(BASE):
    __tablename__ = "nice_discord_bot"
    _id = Column(Integer, autoincrement=True, primary_key=True)
    guild_id = Column(BigInteger)
    user_id = Column(BigInteger)
    nice_count = Column(Integer)

    def __init__(
            self,
            guild_id,
            user_id,
            nice_count):
        self.guild_id = guild_id
        self.user_id = user_id
        self.nice_count = nice_count


NiceData.__table__.create(checkfirst=True)


def inc_or_make_user(guild_id, user_id):
    try:
        user = SESSION.query(NiceData).filter(NiceData.guild_id==guild_id,
                                              NiceData.user_id==user_id).one()
        user.nice_count += 1
    except NoResultFound:
        user = NiceData(guild_id, user_id, 1)
        SESSION.add(user)
    SESSION.commit()
    return user.nice_count
